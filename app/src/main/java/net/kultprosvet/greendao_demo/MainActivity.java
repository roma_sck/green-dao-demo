package net.kultprosvet.greendao_demo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import net.kultprosvet.greendao_demo.db.City;
import net.kultprosvet.greendao_demo.db.CityDao;
import net.kultprosvet.greendao_demo.db.DaoMaster;
import net.kultprosvet.greendao_demo.db.DaoSession;
import net.kultprosvet.greendao_demo.db.Forecast;

import java.util.List;

import de.greenrobot.dao.query.Query;
import static android.provider.ContactsContract.Contacts;
import  static android.provider.ContactsContract.Data.*;
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getContacts();

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "localdb", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession session = daoMaster.newSession();

        CityDao cityDao = session.getCityDao();
        City city=new City();
        city.setName("Zhitomir");
       // cityDao.insert(city);
        Forecast forecast=new Forecast();
        forecast.setMax_temp(30f);
        forecast.setMin_temp(20f);
        forecast.setDescription("Sunny");
        forecast.setCity_id(city.getId());
      //  session.getForecastDao().insert(forecast);

/*
        Query<City> query = cityDao.queryBuilder()
                .where(CityDao.Properties.Name.like("Z%"))
                .build();

        List<City> result = query.list();
        city = result.get(0);
        List<Forecast> forecasts = city.getForecasts();
*/

//        cityDao.deleteByKey(1l);





    }
    public void getContacts(){
      Cursor cursor=getContentResolver().query(
              ContactsContract.Contacts.CONTENT_URI,
              null,
              null,
              null,
              null
      );
      while (cursor.moveToNext()){
          Log.d("Contacts",
                  cursor.getString(cursor.getColumnIndex(Contacts.DISPLAY_NAME_PRIMARY))
                  );


          long rawContactId=cursor.getLong(cursor.getColumnIndex(NAME_RAW_CONTACT_ID));
          Cursor rawCursor = getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                  null,
                  ContactsContract.Data.RAW_CONTACT_ID + "=?" + " AND "
                          + ContactsContract.Data.MIMETYPE + "='" + ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "'",
                  new String[] {String.valueOf(rawContactId)}, null);
            while (rawCursor.moveToNext()){
                Log.d("Contacts ","phone"+rawCursor.getString(rawCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
            }
          rawCursor.close();
      }


      Log.d("Contacts",String.valueOf(cursor.getCount()));
        cursor.close();
    }
}
